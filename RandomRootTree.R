#dhtvtif ( !exists("SuplFuncs")    ) source("./SuplFuncs.R")
SuplFuncs <- list()
SuplFuncs$countLevels <- function(levels, vec) {as.numeric(table(vec)[levels])}

RandomRootTree <- setRefClass("RandomRootTree",
                              fields  = list(rootQuestion      = "integer",
                                             rootAnswers       = "character",
                                             answerType        = "character",
                                             randomRootDistr   = "numeric",
                                             treesArray        = "character",
                                             distrArray        = "array",
                                             logFileName       = "character",
                                             nSamplesInLeaf    = "integer",
                                             isNotPruned       = "logical",
                                             classes           = "character",
                                             attributes        = "character",
                                             samplesInNode     = "array",
                                             className         = "character"
                              ),
                              methods = list()
)

NUMBER_OF_SAMPLES_IN_LEAF <- 1
IS_NOT_PRUNED             <- TRUE

RandomRootTree$methods(
  ## constructor ##
  initialize = function( params = list(), logFileName_ = '', ... )
  {
    'constructor of RandomRootTree'
    
    library("RWeka")
    # number of samples in leaf
    nSamplesInLeaf        <<- as.integer(ifelse(is.null(params$nSamplesInLeaf),  NUMBER_OF_SAMPLES_IN_LEAF, params$nSamplesInLeaf))
    
    # unpurened or pruned
    isNotPruned           <<- as.logical(ifelse(is.null(params$isNotPruned),  IS_NOT_PRUNED, params$isNotPruned))
    
    logFileName           <<- logFileName_
    # check parameters
    stopifnot(nSamplesInLeaf >= 1)
    
    callSuper(...)
  },
  
  # build RRTree
  build = function(trainData, 
                   RRQnum      = as.integer(1),
                   className   = 'class',
                   classes     = as.character(unique(trainData[className])),
                   attributes  = names(trainData)[names(trainData) != "class"],
                   rootAnswers = as.character(levels(trainData[,RRQnum])),
                   ...)
  {
    'build model of RandomRootForest'
    #     logger <- create.logger(logfile = logFileName, log4r:::INFO)
    #     info(logger, "*** Random Root Tree is building.... ***")

    className  <<- className
    classes    <<- classes
    attributes <<- attributes
    
    
    
    # set model tree's attributes
    rootQuestion  <<- RRQnum
    rootAnswers   <<- rootAnswers
    
    ifelse(length(rootAnswers)==0, answerType <<- "Real", answerType <<- "Cartegorical")

    randomRootDistr   <<- SuplFuncs$countLevels(classes, trainData[className])
    randomRootDistr   <<- randomRootDistr / max(1, sum(randomRootDistr))
    

    #
    # fill subtrees for real or categorical num
    if (answerType == "Real")
    { 
      
      # find numeric split
      dModel <- DecisionStump(trainData[[className]] ~ trainData[[rootQuestion]], data = trainData)
      tmpStr <- unlist(strsplit(dModel$classifier$toString(), "<="))[2]
      ansVal <- unlist(strsplit(tmpStr," "))[2];
      
      rootAnswers <<- as.character(ansVal)
      
      distrArray <<- array(list(),dim=2)
      returnStructure <- list()
      
      samplesInNode <<- array(list(),dim=2)
      # get subtrees
      # <= 
      tmpTrain                              <- trainData[!is.na(trainData[[rootQuestion]]),]
      
      tmpTrain                              <- tmpTrain[tmpTrain[[rootQuestion]] <= as.numeric(ansVal),]
      localModel                            <- J48(as.formula(paste(className,"~.",sep="")), data = tmpTrain, control = Weka_control(M = nSamplesInLeaf, U = isNotPruned), na.action=na.pass)
      
      treesArray[1]                         <<- localModel$classifier$graph()
      # distrArray[[1]]                       <<- getDistr(distrArray[[1]], RWeka:::parse_Weka_digraph(treesArray[1]), "N0", tmpTrain, classes)
      
      returnStructure$treeDistr      <- distrArray[[1]]
      returnStructure$samplesInNode  <- samplesInNode[[1]]
      
      returnStructure                <- getDistrAndSamplInNode(returnStructure, RWeka:::parse_Weka_digraph(treesArray[1]), "N0", tmpTrain, classes)
      
      distrArray[[1]]    <<- returnStructure$treeDistr
      samplesInNode[[1]] <<- returnStructure$samplesInNode
      
      # >
      tmpTrain                               <- trainData[!is.na(trainData[[rootQuestion]]),]
      
      tmpTrain                               <- tmpTrain[tmpTrain[[rootQuestion]] > as.numeric(ansVal),]
      localModel                             <- J48(as.formula(paste(className,"~.",sep="")), data = tmpTrain, control = Weka_control(M = nSamplesInLeaf, U = isNotPruned), na.action=na.pass)
      
      treesArray[2]                         <<- localModel$classifier$graph()
      # distrArray[[2]]                       <<- getDistr(distrArray[[2]], RWeka:::parse_Weka_digraph(treesArray[2]), "N0", tmpTrain, classes)
      
      returnStructure$treeDistr      <- distrArray[[2]]
      returnStructure$samplesInNode  <- samplesInNode[[2]]
      
      returnStructure                <- getDistrAndSamplInNode(returnStructure, RWeka:::parse_Weka_digraph(treesArray[2]), "N0", tmpTrain, classes)
      
      distrArray[[2]]    <<- returnStructure$treeDistr
      samplesInNode[[2]] <<- returnStructure$samplesInNode
      
    } else
    {
      nRootAnswers <- length(rootAnswers)
      
      distrArray    <<- array(list(), dim=nRootAnswers)
      samplesInNode <<- array(list(), dim=nRootAnswers)
      returnStructure <- list()
      for (nAns in 1:nRootAnswers)
      {
        tmpTrain                                 <- trainData[!is.na(trainData[[rootQuestion]]),]
        
        tmpTrain                                 <- tmpTrain[tmpTrain[[rootQuestion]] == rootAnswers[nAns],]
        localModel                               <- J48(as.formula(paste(className,"~.",sep="")), data = tmpTrain, control = Weka_control(M = nSamplesInLeaf, U = isNotPruned), na.action=na.pass)
        
        treesArray[nAns]                      <<- localModel$classifier$graph()
        # distrArray[[nAns]]                  <<- getDistr(distrArray[[nAns]], RWeka:::parse_Weka_digraph(treesArray[nAns]), "N0", tmpTrain, classes)
        
        returnStructure$treeDistr      <- distrArray[[nAns]]
        returnStructure$samplesInNode  <- samplesInNode[[nAns]]
        
        returnStructure                <- getDistrAndSamplInNode(returnStructure, RWeka:::parse_Weka_digraph(treesArray[nAns]), "N0", tmpTrain, classes)
        
        distrArray[[nAns]]    <<- returnStructure$treeDistr
        samplesInNode[[nAns]] <<- returnStructure$samplesInNode
      }
      
    }
    #     logger <- create.logger(logfile = logFileName, log4r:::INFO)
    #     info(logger, "*** Random Root Tree is built!***")
    
  },
  
  # get distribution in each tree node
  getDistr=function(treeDistr, tree, nodeLabel, dfTrain, levels)
  {
    dfTrain <- dfTrain[!is.na(dfTrain[className]),]
    
    treeDistr[[nodeLabel]] <- SuplFuncs$countLevels(levels, dfTrain[className])
    treeDistr[[nodeLabel]] <- treeDistr[[nodeLabel]] / max(sum(treeDistr[[nodeLabel]]), 1)
    
    # find kids  
    nodeKids <- findKids(tree, nodeLabel)
    
    # if is leaf than return, else go on moving
    if (is.null(nodeKids))
    {
      return(treeDistr)
    }else
    {
      for (kid in nodeKids)
      {
        newDfTrain   <- splitDataByRuleLabel(dfTrain, tree$nodes[,2][tree$nodes[,1] == nodeLabel], tree$edge[,3][(tree$edge[,1] == nodeLabel) & (tree$edge[,2] == kid)])
        treeDistr    <- getDistr(treeDistr, tree, kid, newDfTrain, levels)
      }
      return(treeDistr)
    }
  },
  # get distribution in each tree node
  getDistrAndSamplInNode = function(returnStructure, tree, nodeLabel, dfTrain, levels)
  {
    
    dfTrainTmp <- dfTrain[!is.na(dfTrain[className]),]
    returnStructure$treeDistr[[nodeLabel]] <- SuplFuncs$countLevels(levels, dfTrainTmp$class)
    returnStructure$treeDistr[[nodeLabel]] <- returnStructure$treeDistr[[nodeLabel]] / max(sum(returnStructure$treeDistr[[nodeLabel]]), 1)
    
    returnStructure$samplesInNode[[nodeLabel]] <- dim(dfTrain)[1]
    # find kids  
    nodeKids <- findKids(tree, nodeLabel)
    
    # if is leaf than return, else go on moving
    if (is.null(nodeKids))
    {
      return(returnStructure)
    }else
    {
      for (kid in nodeKids)
      {
        newDfTrain         <- splitDataByRuleLabel(dfTrain, tree$nodes[,2][tree$nodes[,1] == nodeLabel], tree$edge[,3][(tree$edge[,1] == nodeLabel) & (tree$edge[,2] == kid)])
        returnStructure    <- getDistrAndSamplInNode(returnStructure, tree, kid, newDfTrain, levels)
      }
      return(returnStructure)
    }
  },
  
  # find left kids for the node
  findKids = function(tree, nodeLabel)
  {
    nodeKids <- tree$edge[,2][tree$edge[,1] == nodeLabel]
    if (length(nodeKids) == 0)
    {
      nodeKids <- NULL
    }
    return(nodeKids)
  },
  # split data by a rule and return splitted Nead label here
  splitDataByRuleLabel = function(curData, question, answer)
  {
    tmpStr <- unlist(strsplit(answer, "<=" ))
    if (length(tmpStr) > 1)
    {
      threshold <- as.numeric(tmpStr[2])
      curData <- curData[curData[[question]]  <= threshold,]
    }else
    {
      tmpStr    <- unlist(strsplit(answer, ">" ))
      if (length(tmpStr) > 1)
      {
        threshold <- as.numeric(tmpStr[2])
        curData   <- curData[curData[[question]]  > threshold,]
      } else
      {
        tmpStr    <- unlist(strsplit(answer, " " ))
        threshold <- tmpStr[2]
        len       <- length(tmpStr)
        if ( len > 2)
        {
          for (s in 3:len)
          {
            threshold <- paste(threshold, tmpStr[s])
          }
        }
        curData <- curData[curData[[question]]== threshold,]
      }
    }
    
    return(curData)
  },
  
  getPersonRecomendationNum = function(personDistr, 
                                       treeDistrs)
  {
    recomendationLabelnum <- 1;
    curDistance <- Inf
    for (nKid in 1:length(treeDistrs))
    {
      newDistance <-getDistnacesBetweenDistributions(personDistr[[1]], treeDistrs[[nKid]])
      if (newDistance < curDistance)
      {
        newDistance           <- newDistance
        recomendationLabelnum <- nKid
      }
    }
    return(recomendationLabelnum)
  },
  # get node structure from the tree (nodes)
  getNode = function(tree, 
                     curNode, 
                     curLabel)
  {
    
    answersLabels <- NULL
    nAnswers      <- 0
    nodesAnswes   <- NULL
    answerDldIn   <- " Choose answer: ";
    valAnswers    <- NULL
    
    for (node in curNode:(length(tree)-1))
    {
      if (length(unlist(strsplit(tree[node], paste(curLabel,"->", sep="") ))) > 1)
      {
        nAnswers <- nAnswers + 1
        nodesAnswes <- c(nodesAnswes, node);
        
        # find next label
        tmpStr <- unlist(strsplit(tree[node], "->"))
        tmpStr <- unlist(strsplit(tmpStr[2], " "))
        answersLabels<-c(answersLabels,tmpStr[1])
        
        # fill awnswers
        tmpStr <- unlist(strsplit(tree[node], "\""))
        answerDldIn <- paste(answerDldIn, "\n", nAnswers , ":", tmpStr[2])
        
        valAnswers<- c(valAnswers, tmpStr[2])
      }
      
      # question check
      if (length(unlist(strsplit(tree[node], paste(curLabel," ", sep="") ))) > 1)
      {
        question<-unlist(strsplit(tree[node], "\"" ))[2]
      }
    }
    
    cNode <- NULL
    cNode$answersLabels <- answersLabels
    cNode$nAnswers      <- nAnswers
    cNode$nodesAnswes   <- nodesAnswes
    cNode$answerDldIn   <- answerDldIn
    cNode$valAnswers    <- valAnswers
    cNode$question      <- question
    return(cNode)
  },
  
  moveToQuestionByPositionInTree = function(answers, pointer)
  {
    # move by tree
    # pre trees
    treeObj    <- treesArray[as.integer(answers[rootQuestion])]
    tree       <- unlist(strsplit(treeObj, "\n"))
    
    curLabel   <- pointer$cLabel[pointer$RRQuestionNum]
    curNode    <- pointer$cNode[pointer$RRQuestionNum]
    nNodes     <- length(tree) - 1;
    
    cNode <- getNode(tree, curNode, curLabel)
    
    # unpack
    answersLabels <- cNode$answersLabels
    nAnswers      <- cNode$nAnswers
    nodesAnswes   <- cNode$nodesAnswes
    answerDldIn   <- cNode$answerDldIn
    valAnswers    <- cNode$valAnswers
    question      <- cNode$question
    
    # check leaf
    if (nAnswers == 0)
    {
      pointer$cNode[pointer$RRQuestionNum]  <- curNode
      pointer$cState[pointer$RRQuestionNum] <- "Leaf"
      pointer$cLabel[pointer$RRQuestionNum] <- curLabel
      
      return(pointer)
    }else
    {
      if (is.na(answers[attributes == question]))
      {
        indx <- seq(1,length(attributes),1)
        qNum <- indx[attributes == question]
        
        pointer$currentQuestionNum <- qNum
        
        pointer$cNode[pointer$RRQuestionNum]  <- curNode
        pointer$cState[pointer$RRQuestionNum] <- "Free"
        pointer$cLabel[pointer$RRQuestionNum] <- curLabel
        
        return(pointer)
      }
      
      if (answers[attributes == question] != "Skip")
      {
        labelNum <- as.numeric(answers[attributes == question])
        curNode  <- nodesAnswes[labelNum]
        curLabel <- answersLabels[labelNum]
        
        pointer$cNode[pointer$RRQuestionNum]  <- curNode
        pointer$cState[pointer$RRQuestionNum] <- "Free"
        pointer$cLabel[pointer$RRQuestionNum] <- curLabel
        
        indx <- seq(1,length(attributes),1)
        qNum <- indx[attributes == question]
        
        pointer$currentQuestionNum <- qNum
        
        return(pointer)
      }else{ 
        
        pointer$cNode[pointer$RRQuestionNum]  <- curNode
        pointer$cState[pointer$RRQuestionNum] <- "Skip"
        pointer$cLabel[pointer$RRQuestionNum] <- curLabel
        
        return(pointer)
      }
      
    }
  },
  
  moveToPredictionQuestionByPostionInTree = function(answers, pointer)
  { 
    # move by recomendation
    if (is.na(as.integer(answers[rootQuestion])))
    {   
      # move to prediction
      # set tree statment = Free
      
      treeDistr <- distrArray[1][[1]]$N0
      nAnswers  <- length(distrArray)
      for(ans in 2:nAnswers)
      {
        treeDistr <- rbind(treeDistr, distrArray[ans][[1]]$N0)
      }
      recomendationLabelNum <- SuplFuncs$getPersonRecomendationNumRootFunc(userDistribution, treeDistr)
      pointer$cState[pointer$RRQuestionNum] <- "Free"
      
      answers[rootQuestion] <- recomendationLabelNum
      pointer <- moveToQuestionByPositionInTree(answer, pointer)
      answers[rootQuestion] <- "Skip"
      
    }else
    {
      recomendationLabelNum <- getAnswerRecomendationNumberByPositionInTree(pointer)
      
      labelNum <- as.numeric(recomendationLabelnum)
      curNode  <- nodesAnswes[labelNum]
      curLabel <- answersLabels[labelNum]
      
      cNode      <- getNode(tree, curNode, curLabel)
      
      # unpack
      answersLabels <- cNode$answersLabels
      nAnswers      <- cNode$nAnswers
      nodesAnswes   <- cNode$nodesAnswes
      answerDldIn   <- cNode$answerDldIn
      valAnswers    <- cNode$valAnswers
      question      <- cNode$question
      
      # check leaf
      if (nAnswers == 0)
      {    
        pointer$cNode[pointer$RRQuestionNum]  <- curNode
        pointer$cState[pointer$RRQuestionNum] <- "Leaf"
        pointer$cLabel[pointer$RRQuestionNum] <- curLabel
        
      }else
      {
        pointer$cNode[pointer$RRQuestionNum]  <- curNode
        pointer$cState[pointer$RRQuestionNum] <- "Free"
        pointer$cLabel[pointer$RRQuestionNum] <- curLabel
        
        indx <- seq(1,length(attributes),1)
        qNum <- indx[attributes == question]
        
        pointer$currentQuestionNum <- qNum
      }
    }
    return(pointer)
  },
  
  getAnswerRecomendationNumberByPositionInTree = function(userDistribution, answers, pointer)
  {
    # check if the question is a root question
    if (is.na(as.integer(answers[rootQuestion])))
    {   
      treeDistr <- distrArray[1][[1]]$N0
      nAnswers  <- length(distrArray)
      for(ans in 2:nAnswers)
      {
        treeDistr <- rbind(treeDistr, distrArray[ans][[1]]$N0)
      }
      recomendationLabelNum <- SuplFuncs$getPersonRecomendationNumRootFunc(userDistribution, treeDistr)
    }else
    {
      treeObj <- treesArray[as.integer(answers[rootQuestion])]
      
      parsedTree <- RWeka:::parse_Weka_digraph(treeObj);
      nodeKids   <- findKids(parsedTree, pointer$cLabel[pointer$RRQuestionNum])
      
      treeDistr  <- distrArray[as.integer(answers[rootQuestion])]
      recomendationLabelNum <- SuplFuncs$getPersonRecomendationNum(userDistribution, treeDistr[[1]][nodeKids])
      
    }
  },
  
  plotToFile = function(fileName = "Tree", 
                        metaData = "", 
                        fileType = "svg", 
                        dotExeLocation = getwd())
  {
    # pre plot. set inits
    tmpDotFileName <- paste(dotExeLocation, "/tmp.gv",     sep = "")
    batFileName    <- paste(dotExeLocation, "/dotRun.bat", sep = "")
    cmd            <- paste(dotExeLocation, "/dot.exe -T", fileType, " ", tmpDotFileName, " > ", fileName, ".", fileType, sep="")
    
    # write to the dot-file
    fileString <- paste("digraph rootQuestionTree", rootQuestion, " {\n", sep = "")
    
    if (class(metaData)!="MetaData")
    {
      fileString <- paste(fileString, "R", rootQuestion, " [label = \"", attributes[rootQuestion], "\"]\n",sep = "")
      # write root
      if (answerType ==  "Cartegorical")
      {
        for (treeNum in 1:length(treesArray))
        {
          subLabel   <- paste("t", treeNum, "_N0", sep="")
          fileString <- paste(fileString, "R", rootQuestion,"-> ", subLabel, " [label = \"=", rootAnswers[treeNum], "\"]\n", sep="")
        }
      }else{
        subLabel   <- paste("t", 1, "_N0", sep="")
        fileString <- paste(fileString, "R", rootQuestion,"-> ", subLabel, " [label = \"<=", rootAnswers[1], "\"]\n", sep="")
        
        subLabel   <- paste("t", 2, "_N0", sep="")
        fileString <- paste(fileString, "R", rootQuestion,"-> ", subLabel, " [label = \">", rootAnswers[1], "\"]\n", sep="")
        
      }
      # write subtrees
      for (treeNum in 1:length(treesArray))
      {
        subLabel <- paste("t", treeNum, "_N\\2", sep="")
        
        tmpStr    <- strsplit(treesArray[treeNum], "digraph J48Tree \\{\n")[[1]][2]
        tmpStr    <- strsplit(tmpStr, "\\}")[[1]][1]
        tmpStr    <- gsub("(N)(\\d)",subLabel,tmpStr)
        
        fileString <- paste(fileString, tmpStr)
      }
      
    }else
    {
      fileString <- paste(fileString, "R", rootQuestion, " [label = \"", metaData[rootQuestion]$question, "\"]\n",sep = "")
      # write root
      if (answerType ==  "Cartegorical")
      {
        for (treeNum in 1:length(treesArray))
        {
          subLabel   <- paste("t", treeNum, "_N0", sep="")
          ansNumber  <- as.numeric(strsplit(rootAnswers[treeNum],"a")[[1]][2])
          fileString <- paste(fileString, "R", rootQuestion,"-> ", subLabel, " [label = \"", metaData[rootQuestion]$answers[[ansNumber]]$answer, "\"]\n", sep="")
        }
      }else{
        subLabel   <- paste("t", 1, "_N0", sep="")
        fileString <- paste(fileString, "R", rootQuestion,"-> ", subLabel, " [label = \"<=", rootAnswers[1], "\"]\n", sep="")
        
        subLabel   <- paste("t", 2, "_N0", sep="")
        fileString <- paste(fileString, "R", rootQuestion,"-> ", subLabel, " [label = \">", rootAnswers[1], "\"]\n", sep="")
        
      }
      # write subtrees
      for (treeNum in 1:length(treesArray))
      {
        subLabel <- paste("t", treeNum, "_N\\2", sep="")
        
        parsedTree <- RWeka:::parse_Weka_digraph(treesArray[treeNum])
        parsedTree <- fillParsedTreeByMetaData(parsedTree, metaData, "N0")
        
        parsedTreeAsTreeString <- parsedTreeToTreeString(parsedTree, distrArray[[treeNum]], samplesInNode[[treeNum]], metaData)
        
        tmpString                 <- strsplit(parsedTreeAsTreeString, "digraph J48Tree")[[1]][2]
        parsedTreeAsTreeString    <- strsplit(tmpString, "\\{\n")[[1]][2]
        
        parsedTreeAsTreeString    <- substr(parsedTreeAsTreeString, 1,nchar(parsedTreeAsTreeString)-4)
        parsedTreeAsTreeString    <- gsub("(N)(\\d)",subLabel,parsedTreeAsTreeString)
        
        fileString <- paste(fileString, parsedTreeAsTreeString)
        
      }
      
    }
    
    fileString <- paste(fileString, paste("}\n", sep = ""))
    write(fileString, file = tmpDotFileName)
    write(cmd, batFileName)
    system(batFileName ,invisible = FALSE, wait = FALSE)
    
  },
  
  parsedTreeToTreeString = function(parsedTree, distribution, smplInNode, metaData)
  {
    treeString <- paste("digraph J48Tree \\{\n", sep = "")
    # write nodes
    for ( nodeNum in 1:dim(parsedTree$nodes)[1] )
    {
      splitVar <- parsedTree$nodes[nodeNum,"splitvar"]
      if (splitVar == "")
      {
        
        lenOfCategoryInfoStr <- 20
        lenOfNumber          <- 3
        
        inds <- order(distribution[[parsedTree$nodes[nodeNum,"name"]]], decreasing = TRUE)
        categoryInfoStrings <- ""
        for (ind in inds)
        {
          if (distribution[[parsedTree$nodes[nodeNum,"name"]]][ind] > 0)
          {
            categoryInfoStrings <- c(categoryInfoStrings, paste(substr(metaData[classes[ind]]$question,1,lenOfCategoryInfoStr), round(distribution[[parsedTree$nodes[nodeNum,"name"]]][ind], digits = lenOfNumber) ))
          }
          
        }
        
        categoryInfoStrings <- categoryInfoStrings[-1]
        
        nCategoryInfoStrings <- length(categoryInfoStrings)
        
        catLeafStr <- "{"
        if (nCategoryInfoStrings > 1)
        {
          for (class in 1:(nCategoryInfoStrings))
          {
            catLeafStr      <- paste(catLeafStr, categoryInfoStrings[class], "|", sep = "")
          }
          
        }
        treeString <- paste(treeString, parsedTree$nodes[nodeNum,"name"], "[label = \"",catLeafStr , smplInNode[[parsedTree$nodes[nodeNum,"name"]]]," } \" shape=record style=filled] \n",sep = "")
        
      }else
      {
        treeString <- paste(treeString, parsedTree$nodes[nodeNum,"name"], "[label = \"", splitVar ,"\"] \n",sep = "")
      }
    }
    
    # write edge
    for ( edgeNum in 1:dim(parsedTree$edge)[1] )
    {
      treeString <- paste(treeString, parsedTree$edge[edgeNum,"from"], "-> ", parsedTree$edge[edgeNum,"to"], "[label = \"", parsedTree$edge[edgeNum,"label"] , "\"shape=box style=filled] \n",sep = "")
    }
    
    treeString <- paste(treeString, "}\n", sep = "")
    return(treeString)
  },
  
  # get distribution in each tree node
  fillParsedTreeByMetaData = function(parsedTree, metaData, nodeLabel)
  {
    # change
    
    # find kids  
    nodeKids <- findKids(parsedTree, nodeLabel)
    
    if ( is.null(nodeKids) )
    {
      return(parsedTree)
    }else{
      
      # change node question to the metadata question
      nodeInTreePosition                               <- parsedTree$nodes[,"name"] == nodeLabel
      questionLabel                                    <- parsedTree$nodes[nodeInTreePosition, 'splitvar']
      
      # change node title to the node question
      parsedTree$nodes[nodeInTreePosition, 'splitvar'] <- metaData[questionLabel]$question
      
      # kids loop
      for (kid in nodeKids)
      {  
        # fill edge
        if (metaData[questionLabel]$question_type == "discrete_single")
        {
          edgeIndex <- (parsedTree$edge[, 'from'] == nodeLabel & parsedTree$edge[, 'to'] == kid)
          edgeNum   <- as.numeric(strsplit(parsedTree$edge[edgeIndex, 'label'],"= ")[[1]][2])
          parsedTree$edge[edgeIndex, 'label'] <-  metaData[questionLabel]$answers[[edgeNum]]$answer
        }
        
        parsedTree <- fillParsedTreeByMetaData(parsedTree, metaData, kid)
      }
      
    }
    
    return(parsedTree)
  }
  
)

# class test
# classTest = FALSE
classTest = TRUE
if (classTest)
{
  
  rrt <- RandomRootTree$new(params = list(nSamplesInLeaf = 1) )
  trainData <- read.csv("labor-negotiations.data",header=T)
  
  # add categorical variable to train-data
  rrt$build(trainData  = trainData,
            attributes = names(trainData)[names(trainData) != 'class'],
            className  = 'class',
            RRQnum     = as.integer(2))
   
 rrt$plotToFile(fileName = "Tree")
}
